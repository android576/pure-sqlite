package ua.com.user576.peopledb

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import kotlin.concurrent.thread

class MainActivity : AppCompatActivity() {

    private lateinit var peopleDbHelper: PeopleDbHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        peopleDbHelper = PeopleDbHelper(applicationContext)
    }

    fun onBtnClick(view : View) {
        when(view.id) {
            R.id.select_all_btn -> {
                println("asdf: Select all rows from table")
                findViewById<TextView>(R.id.table_content).text = peopleDbHelper.selectAll()
            }
            R.id.insert -> {
                println("asdf: Insert row in table")
                peopleDbHelper.insertNewHuman()
            }
            R.id.count -> {
                println("asdf: btn 3")
                thread {
                    val count = peopleDbHelper.getPeopleNumber()
                    println("asdf: count = $count")
                }
            }
        }

    }
}