package ua.com.user576.peopledb

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.database.sqlite.SQLiteStatement
import android.provider.BaseColumns
import java.lang.StringBuilder


class PeopleDbHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(SQL.CREATE_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        if (oldVersion < 2) {
            db.execSQL(SQL.UPDATE_TO_SECOND_VERSION)
        }
    }

    fun getPeopleNumber() : Int {
        val sql = "SELECT COUNT(*) FROM $PEOPLE_TABLE_NAME"
        val statement: SQLiteStatement = readableDatabase.compileStatement(sql)
        val count = statement.simpleQueryForLong()
        return count.toInt()
    }

    fun insertNewHuman() {
        val index = getPeopleNumber()
        val name = "name$index"
        val surname = "surname$index"
        val address = "address$index"

        // Create a new map of values, where column names are the keys
        val values = ContentValues().apply {
            put(PeopleTable.COLUMN_NAME, name)
            put(PeopleTable.COLUMN_SURNAME, surname)
            put(PeopleTable.COLUMN_ADDRESS, address)
        }

        // Insert the new row, returning the primary key value of the new row
        writableDatabase?.insert(PeopleTable.TABLE_NAME, null, values)
    }

    fun selectAll() : String {
        val cursor = readableDatabase.rawQuery("SELECT * FROM $PEOPLE_TABLE_NAME", null)
        val strBuilder = StringBuilder()
        with(cursor) {
            while (moveToNext()) {

                val id = getLong(0)
                val name = getString(1)
                val surname = getString(2)
                val address = getString(3)

                strBuilder.append("id: $id; name: $name; surname: $surname; "
                        + "address: $address\n"
                )
            }
        }
        cursor.close()

        return strBuilder.toString()
    }

    companion object {
        // If you change the database schema, you must increment the database version.
        private const val DATABASE_VERSION = 2
        private const val DATABASE_NAME = "people.db"
        private const val PEOPLE_TABLE_NAME = PeopleTable.TABLE_NAME

        // Table contents are grouped together in an anonymous object.
        object PeopleTable : BaseColumns {
            const val TABLE_NAME = "people"
            const val COLUMN_NAME = "name"
            const val COLUMN_SURNAME = "surname"
            const val COLUMN_ADDRESS = "address"
            const val ADDRESS_TYPE = "TEXT NOT NULL DEFAULT ''"
        }

        // SQL statements are placed here
        object SQL {
            const val CREATE_TABLE = "CREATE TABLE ${PeopleTable.TABLE_NAME} (" +
            "${BaseColumns._ID} INTEGER PRIMARY KEY," +
            "${PeopleTable.COLUMN_NAME} TEXT," +
            "${PeopleTable.COLUMN_SURNAME} TEXT, " +
            "${PeopleTable.COLUMN_ADDRESS} ${PeopleTable.ADDRESS_TYPE})"

            const val UPDATE_TO_SECOND_VERSION =
                "ALTER TABLE ${PeopleTable.TABLE_NAME} " +
                        "ADD ${PeopleTable.COLUMN_ADDRESS} ${PeopleTable.ADDRESS_TYPE}};"
        }
    }
}
