package ua.com.user576.peopledb

import android.content.ContentValues
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.Assert.assertEquals

@RunWith(AndroidJUnit4::class)
class DatabaseTest {

    private lateinit var peopleDbHelper: PeopleDbHelper

    @Before
    fun setUp() {
        peopleDbHelper = PeopleDbHelper(
            InstrumentationRegistry.getInstrumentation().targetContext
        )
        peopleDbHelper.writableDatabase.delete("people", null, null)
    }

    @After
    fun finish() {
        peopleDbHelper.close()
    }

    @Test
    fun writeReadTest() {
        val contentValues = ContentValues().apply {
            put("name", "Ivan")
            put("surname", "Ivanov")
            put("address", "Moscow")
        }
        peopleDbHelper.writableDatabase.insert("people", null, contentValues)
        print(
            peopleDbHelper.selectAll()
        )
        assertEquals(
            "id: 1; name: Ivan; surname: Ivanov; address: Moscow\n",
            peopleDbHelper.selectAll()
        )
    }
}
